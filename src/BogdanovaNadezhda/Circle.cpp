#include "Circle.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pbRadius_clicked()
{
    QString r=ui->laR->text();
    radius=r.toDouble();
    ference=2*3.14159*radius;
    square=3.14159*radius*radius;
    ui->laF->setText(QString::number(ference));
    ui->laS->setText(QString::number(square));
}
