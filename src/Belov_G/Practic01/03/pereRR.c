// ��������� pereRR.c ��������� �������� ���� �� �������� � ������� � ��������
#include <stdio.h>
#include <locale.h>
#define PI 3.14159265

int main()
{
	setlocale(LC_ALL, "");

	float dr;
	char symbol; // ����� ���� �������� ���������� �� ���������� �������: D - �������, R - �������

	printf("������� �������� D/R: ");
	if (scanf("%f%c", &dr, &symbol) == 2 && (symbol == 'D' || symbol == 'R'))  // �������� ������������ �����
	 { 
		if (symbol == 'D')
		{
			printf(" %.2fD = %.2fR\n", dr, dr * (PI / 180.0));
		}
		else printf(" %.2fR = %.2fD\n", dr, dr * (180.0 / PI));
	 }
	else printf(" ������\n");
	return 0;
}